import React, { useState, useEffect } from "react";
import axios from "axios";
import "./style.css";
import ProductList from "./ProductList";
const App = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [authenticated, setAuthenticated] = useState(false);

  useEffect(() => {
    // Check user authentication status on component mount
    // You might want to fetch this information from your backend
    // For simplicity, we'll use a mock function here
    checkAuthenticationStatus();
  }, []);

  const checkAuthenticationStatus = () => {
    // Mock function, replace with actual authentication logic
    // For example, you might make a request to your backend to check if the user is authenticated
    const isAuthenticated = localStorage.getItem("token") ? true : false;
    setAuthenticated(isAuthenticated);
  };

  const handleLogin = async (event) => {
    event.preventDefault();
    try {
      // Mock API endpoint, replace with your actual login endpoint
      const response = await axios.post("https://dummyjson.com/auth/login", {
        username: username,
        password: password,
      });

      const token = response.data.token;
      localStorage.setItem("token", token);
      setAuthenticated(true);
    } catch (error) {
      console.error("Login failed:", error);
    }
  };

  const handleLogout = () => {
    // Mock function, replace with actual logout logic
    // For example, you might make a request to your backend to log the user out
    localStorage.removeItem("token");
    setAuthenticated(false);
  };

  return (
    <div className="App">
      {authenticated ? (
        <>
          <ProductList />
          <h2>Welcome, User!</h2>
          <button className=" btn btn-danger" onClick={handleLogout}>
            Logout
          </button>
        </>
      ) : (
        <form className="thon" onSubmit={handleLogin}>
          <label className=" p-1">
            Username:
            <input
              type="text"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </label>
          <br />
          <label className=" p-1">
            Password:
            <input
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </label>
          <br />
          <button className="btn btn-close-white bg-info" type="submit">
            Login
          </button>
        </form>
      )}
    </div>
  );
};

export default App;
